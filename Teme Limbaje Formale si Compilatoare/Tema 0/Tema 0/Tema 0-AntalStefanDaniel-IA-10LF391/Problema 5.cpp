#include<iostream>
#include<string>
#include<vector>
#include<algorithm>
void Input(int16_t& numberOfWords, std::vector<std::string> &listOfWords)
{
	std::cout << "Introduceti numarul de cuvinte pe care doriti sa le cititi.\n";
	std::cin >> numberOfWords;
	std::cout << "Introduceti cuvintele.\n";
	for (int index = 0; index < numberOfWords; index++)
	{
		std::string word;
		std::cin >> word;
		listOfWords.push_back(word);
	}
}
void Out(const int16_t& numberOfWords, const std::vector<std::string> &listOfWords)
{
	std::cout << "Cuvintele sortate in oridine crescatoare sunt: \n";
	for (int index = 0; index < numberOfWords; index++)
	{
		std::cout << index + 1 << ". " << listOfWords[index] << std::endl;
	}
}
int main()
{
	int16_t numberOfWords;
	std::vector<std::string> listOfWords;
	Input(numberOfWords, listOfWords);
	std::sort(listOfWords.begin(), listOfWords.end());
	Out(numberOfWords, listOfWords);
	return 0;
}