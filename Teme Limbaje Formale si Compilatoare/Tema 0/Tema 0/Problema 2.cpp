#include<iostream>
#include<string>
#include<regex>
void IsNumber(const std::string& number)
{
	std::regex naturalNumber{ R"((\d)+)" };
	std::regex integer{ R"(\-(\d)+)" };
	std::regex realNumber{ R"(((\d)+\.(\d)+))" };
	std::regex realNumberExponentialForm{ R"((\-)?((\d)+(\.(\d)+)*E\-(\d)+))" };
	if (std::regex_match(number, naturalNumber))
		std::cout << number << " este numar natural.\n";
	else
		if (std::regex_match(number, integer))
			std::cout << number << " este numar intreg.\n";
		else
			if (std::regex_match(number, realNumber))
				std::cout << number << " este numar real.\n";
			else
				if (std::regex_match(number, realNumberExponentialForm))
					std::cout << number << " este numar real in forma exponentiala.\n";
				else
					std::cout << number << " nu este numar";

}
int main()
{
	std::string number;
	std::cout << "Introduceti numarul dorit.\n";
	std::getline(std::cin, number);
	IsNumber(number);
	return 0;
}