#include<iostream>
#include<string>
#include<vector>
#include<sstream>
#include<algorithm>
void SplitPhrase(const std::string& phrase, std::vector<std::string>& words)
{
	if (phrase.length() == 0)
	{
		std::cout << "Fraza nu contine cuvinte.\n";
		return;
	}
	else
	{
		std::istringstream token(phrase);
		std::string word;
		while (std::getline(token, word, ' '))
		{
			words.push_back(word);
		}
	}
}
void DeleteChars(std::string& phrase)
{
	int index = 0;
	while(index < phrase.length())
	{
		if (!(std::isalpha(phrase[index])) && phrase[index] != ' ')
			phrase.erase(phrase.begin() + index);
		else
			index++;
	}

}
void DisplayWords(const std::vector<std::string>& words)
{
	for (int index = 0; index < words.size(); index++)
	{
		std::cout << index + 1 << ". " << words[index] << "\n";
	}
}
int main()
{
	std::string phrase;
	std::vector<std::string>words;
	int16_t counter;
	std::cout << "Introduceti fraza pe care doriti sa o impartiti in cuvinte.\n";
	std::getline(std::cin, phrase);
	DeleteChars(phrase);
	SplitPhrase(phrase, words);
	DisplayWords(words);
	return 0;
}