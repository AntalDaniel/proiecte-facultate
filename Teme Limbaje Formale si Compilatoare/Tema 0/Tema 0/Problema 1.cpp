#include<iostream>
#include<string>

int main()
{
	std::string myString;
	char character;
	int16_t counter;
	std::cout << "Introduceti sirul de caractere dorit.\n";
	std::getline(std::cin,myString);
	std::cout << "Introduceti caracterul pe care diriti sa il cautati.\n";
	std::cin >> character;
	counter = std::count(myString.begin(), myString.end(), character);
	std::cout <<"Caracterul "<<character<<" apare de "<<counter<<" ori.\n";
	
	return 0;
}