#include<iostream>
#include<string>
#include<vector>
void Input( std::vector<std::string>& listOfWords,std::string &subString)
{
	int16_t numberOfWords;
	std::cout << "Introduceti numarul de cuvinte pe care doriti sa le cititi.\n";
	std::cin >> numberOfWords;
	std::cout << "Introduceti cuvintele.\n";
	for (int index = 0; index < numberOfWords; index++)
	{
		std::string word;
		std::cin >> word;
		listOfWords.push_back(word);
	}
	std::cout << "Introduceti subsirul.\n";
	std::cin >> subString;
}
std::vector<std::string> ConstructNewList(const std::vector<std::string>& listOfWords,const std::string& subString)
{
	std::vector<std::string> newListOfWords;
	for (int index = 0; index < listOfWords.size(); index++)
	{
		if (listOfWords[index].find(subString) != std::string::npos)
		{
			newListOfWords.push_back(listOfWords[index]);

		}
		
	}
	return newListOfWords;
}
void Out( const std::vector<std::string>& listOfWords)

{
	std::cout << "Cuvintele care contin subsirul introdus sunt: \n";
	for (int index = 0; index < listOfWords.size(); index++)
	{
		std::cout << index + 1 << ". " << listOfWords[index] << std::endl;
	}
}
int  main()
{
	std::vector<std::string> listOfWords;
	std::vector<std::string> newListOfWords;
	std::string subString; 
	Input(listOfWords, subString);
	newListOfWords=ConstructNewList(listOfWords, subString);
	if (newListOfWords.size() == 0)
		std::cout << "Nu exista cuvinte care sa contina subsirul citit de la tastatura.\n";
	else
		Out(newListOfWords);
	return 0;
}