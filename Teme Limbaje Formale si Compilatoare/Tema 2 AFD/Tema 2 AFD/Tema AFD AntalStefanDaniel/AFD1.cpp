#include "AFD1.h"

void AFD1::Citire()
{
	std::ifstream fileInput("Input.txt");
	if (fileInput.is_open())
	{
		int numberOfState;
		int numberOfFinalStates;
		std::string aux;
		std::string index;
		fileInput >> numberOfState;
		for (int index = 0; index < numberOfState; index++)
		{
			fileInput >> aux;
			m_stari.push_back(aux);
		}
		fileInput >> m_sigma;
		fileInput >> m_stareInitiala;
		fileInput >> numberOfFinalStates;
		for (int index = 0; index < numberOfFinalStates; index++)
		{
			fileInput >> aux;
			m_finale.push_back(aux);
		}
		while (!fileInput.eof())
		{
			fileInput >> index;
			fileInput >> aux;
			m_delta.insert(make_pair(index, aux));
		}


	}
	else
		std::cout << "Nu s-a putut deschide fisierul!\n";
	fileInput.close();
}

void AFD1::Afisare()
{
	std::cout << "Automatul este: \n";
	std::cout << "Stari={";
	for (auto stare : m_stari)
	{
		std::cout << stare << ", ";
	}
	std::cout << "}\n";
	std::cout << "Sigma={" << m_sigma << "}\n";
	std::cout << "Stare Initiala={" << m_stareInitiala << "}\n";
	std::cout << "Stari finale={";
	for (auto stareFinala : m_finale)
	{
		std::cout << stareFinala << ", ";
	}
	std::cout << "}\n";
	std::cout << "Delta: \n";
	for (auto tranzition : m_delta)
	{
		std::string aux = tranzition.first;
		char chr= aux.back();
		aux.pop_back();
		std::cout << aux << ' ' << chr << ' ' << tranzition.second<<std::endl;
	}
}

bool AFD1::VerificareCuvant(const std::string& cuvant)
{
	std::string index=m_stareInitiala;
	std::string aux;
	bool isWordOK=false;
	for (auto character : cuvant)
	{
		index.push_back(character);
		if (m_delta.find(index) == m_delta.end())
			throw "Cuvantul a ajuns la blocaj!\n";
		else
		{
			aux=m_delta[index];
			index.clear();
			index = aux;
		}
	}
	for (auto finalState : m_finale)
	{
		if (finalState.find(index) != std::string::npos)
			isWordOK = true;
		
	}
	if(isWordOK==false)
		throw "Cuvantul nu este acceptat!\n";
	return isWordOK;
}

