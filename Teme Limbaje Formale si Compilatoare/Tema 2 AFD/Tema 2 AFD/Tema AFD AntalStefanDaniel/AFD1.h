#pragma once
#include<iostream>
#include<vector>
#include<string>
#include<unordered_map>
#include<fstream>
class AFD1
{
public: 
	void Citire();
	void Afisare();
	bool VerificareCuvant(const std::string &cuvant);

private:
	std::vector<std::string> m_stari;
	std::string m_sigma;
	std::unordered_map<std::string, std::string> m_delta;
	std::string m_stareInitiala;
	std::vector<std::string> m_finale;


};

