﻿#pragma once
#include<iostream>
#include<string>
#include<vector>
#include<fstream>
#include<random>
#include<time.h>
//•	Se pot citi din fișier VN, VT, S, și P astfel: se pun pe prima linie elementele din VN, pe a doua elementele din VT 
//și pe a treia simbolul. Apoi se poate pune numărul de producții și pentru fiecare producție stanga spațiu dreapta.
//•	Pentru afișare : o producție poate fi afișată astfel S->abS
//•	VN și VT pot fi considerați vectori de caractere, S poate fi considerat de tip caracter
//•	Pentru producții se pot folosi doi vectori de string - uri -
//unul de părți stângi și altul de părți drepte sau o clasă / structură - producție
//•	pentru lambda se poate utiliza un caracter rezervat
//•	pentru punctul 4 - generarea unui cuvânt aplicând în mod aleator reguli se dă un exemplu mai jos

class Gramatici
{ public:
	Gramatici();
	void Citire();
	void Afisare();
	bool Verificare();
	void CheckProductionThatCanBeApplied(const std::string& newWord, std::vector<int16_t>&productionIndexList);
	void Generare(const bool& optiune);
private:
	std::string VN;
	std::string VT;
	char S;
	std::vector<std::string> prodLeft;
	std::vector<std::string>prodRight;

};

