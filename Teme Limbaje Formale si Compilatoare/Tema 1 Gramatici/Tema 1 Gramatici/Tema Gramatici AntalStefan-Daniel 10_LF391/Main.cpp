#include"Gramatici.h"

int main()
{
	Gramatici Gramatica;
	Gramatica.Citire();
	Gramatica.Afisare();
    int16_t number;
	bool correct;
	int16_t option;
	do
	{
		std::cout << "Alegeti optiunea pe care doriti sa o afisati!\n";
		std::cout << "0.Afiseaza doar un cuvant generat, fara sa afiseze pasi intermediari.\n";
		std::cout << "1.Afiseaza doar un cuvant generat, afisand si pasii intermediari de formare a cuvantului.\n";
		std::cout << "2.Afiseaza un sir de cuvinte generate, fara afisarea pasilor intermediari.\n";
		std::cout << "3.Afiseaza un sir de cuvinte generate, afisand si pasii intermediari de formare al cuvintelor.\n";
		std::cout << "4.Exit.\n";
		std::cout << "Introduceti optiunea:";
		std::cin >> option;
		std::cout << std::endl;
		switch (option)
		{
		case 0:

			 correct = false;
			try
			{
				correct = Gramatica.Verificare();
			}
			catch (const char* mesajEroare)
			{
				std::cout << "EROARE!!!\n" << mesajEroare;
			}
			if (correct == true)
			{
				Gramatica.Generare(false);
			}
			break;
		case 1:
			 correct = false;
			try
			{
				correct = Gramatica.Verificare();
			}
			catch (const char* mesajEroare)
			{
				std::cout << "EROARE!!!\n" << mesajEroare;
			}
			if (correct == true)
			{
				Gramatica.Generare(true);
			}
			break;
		case 2:
			std::cout << "Introduceti numarul de cuvinte pe care doriti sa le generati:";
			
			std::cin >> number;
			 correct = false;
			try
			{
				correct = Gramatica.Verificare();
			}
			catch (const char* mesajEroare)
			{
				std::cout << "EROARE!!!\n" << mesajEroare;
			}
			if (correct == true)
			{for(int index=0;index<number;index++)
				Gramatica.Generare(false);
			}
			break;

		case 3:
			std::cout << "Introduceti numarul de cuvinte pe care doriti sa le generati:";
			std::cin >> number;
			 correct = false;
			try
			{
				correct = Gramatica.Verificare();
			}
			catch (const char* mesajEroare)
			{
				std::cout << "EROARE!!!\n" << mesajEroare;
			}
			if (correct == true)
			{
				for (int index = 0; index < number; index++)
					Gramatica.Generare(true);
			}
			break;
		case 4:
			std::cout << "Programul s-a inchis.\n";
			break;
		default:
			std::cout << "Optiunea introdusa este gresita!!!\n";
			std::cout << "Introduceti alta optiunea 0 pentru a restarta meniul.";
			std::cin >> option;
			break;
		}
		
	} while (option < 4);
	return 0;
}