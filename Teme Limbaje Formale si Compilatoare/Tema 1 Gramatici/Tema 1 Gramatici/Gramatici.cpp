#include "Gramatici.h"

Gramatici::Gramatici()
{
	srand(time(NULL));
	this->S = NULL;
}

void Gramatici::Citire()
{
	std::ifstream fileInput("Input.txt");
	if (fileInput.is_open())
	{
		std::getline(fileInput, VN);
		std::getline(fileInput, VT);
		fileInput >> S;
		int16_t numberOfProd;
		fileInput >> numberOfProd;
		std::string productions;
		for (int index = 0; index < numberOfProd; index++)
		{
			fileInput >> productions;
			prodLeft.push_back(productions);
			fileInput >> productions;
			prodRight.push_back(productions);
		}
	}
	else
		std::cout << "Eroare, nu s-a putut deschide fisierul cu datele de intrare!\n";
	fileInput.close();
}

void Gramatici::Afisare()
{
	std::cout << "Gramatica este urmatoarea: \n";
	std::cout << "VN = { ";
	for (char index : VN)
	{
		std::cout << index << ", ";
	}
	std::cout << "}\n";
	std::cout << "VT = { ";
	for (char index : VT)
	{
		std::cout << index << ", ";
	}
	std::cout << "}\n";
	std::cout << "Simbolul de start = " << S << "\n";
	std::cout << "Numarul de productii este: " << prodLeft.size() << "\n";
	std::cout << "Productiile sunt: \n";
	for (int16_t index = 0; index < prodLeft.size(); index++)
	{
		std::cout << prodLeft[index] << " ==> " << prodRight[index] << std::endl;
	}


}

bool Gramatici::Verificare()
{

	if (VN.find(S) == std::string::npos)
	{
		throw "S nu apartine multimii neterminalelor!\n";
	}
	for (char character : VN)
	{
		if (VT.find(character) != std::string::npos)
			throw "Intersectia lui VN cu VT nu este vida!\n";
	}
	bool isStart = false;
	for (uint16_t index = 0; index < prodLeft.size(); index++)
	{
		bool isProdInLeft = false;
		bool existNonterminal = false;
		for (char character : prodLeft[index])
		{
			if (VN.find(character) != std::string::npos)
				existNonterminal = true;
			if ((character == S) && (prodLeft[index].length() == 1))
				isStart = true;
			if (VN.find(character) != std::string::npos)
			{
				isProdInLeft = true;
			}
			else
			{
				if (VT.find(character) != std::string::npos)
					isProdInLeft = true;
				else
				{
					isProdInLeft = false;
					break;
				}
			}
		}
		if (isProdInLeft == false)
			throw"Productiile nu contin termeni din VN sau VT!\n";
		if (existNonterminal == false)
			throw "Membrul stang al unei productii nu contine terminali!\n";
		if (isStart == false)
			throw "Nu exista productie al carei membru stang sa contina doar S!\n";
	}
	for (uint16_t index = 0; index < prodRight.size(); index++)
	{
		bool isProdInRight = false;
		for (char character : prodRight[index])
		{
			if (VN.find(character) != std::string::npos)
			{
				isProdInRight = true;
			}
			else
			{
				if (VT.find(character) != std::string::npos)
					isProdInRight = true;
				else
				{
					isProdInRight = false;
					break;
				}
			}
		}
		if (isProdInRight == false)
			throw"Productiile nu contin termeni din VN sau VT!\n";
	}
	std::cout << "Gramatica este corect definita! \n";
	return true;

}

void Gramatici::CheckProductionThatCanBeApplied(const std::string& newWord, std::vector<int16_t>& productionIndexList)
{
	productionIndexList.clear();
	for (int16_t index = 0; index < prodLeft.size(); index++)
	{
		if (newWord.find(prodLeft[index]) != std::string::npos)
			productionIndexList.push_back(index);
	}
}


void Gramatici::Generare(const bool& optiune)
{
	std::string newWord;
	newWord.append(&S);

	std::vector<int16_t>productionIndexList;
	CheckProductionThatCanBeApplied(newWord, productionIndexList);
	while (!productionIndexList.empty())
	{
		int16_t indexProduction = std::rand() % productionIndexList.size();
		std::size_t pozition = newWord.find(prodLeft[productionIndexList[indexProduction]]);
		if (optiune == true)
			std::cout << newWord << "==>";
		if (prodRight[productionIndexList[indexProduction]] != "0")
			newWord.replace(newWord.begin() + pozition, newWord.begin() + pozition + prodLeft[productionIndexList[indexProduction]].length(), prodRight[productionIndexList[indexProduction]]);
		else
			newWord.erase(newWord.begin() + pozition, newWord.begin() + pozition + prodLeft[productionIndexList[indexProduction]].length());
		CheckProductionThatCanBeApplied(newWord, productionIndexList);
	}
	std::cout << newWord << std::endl;
}
