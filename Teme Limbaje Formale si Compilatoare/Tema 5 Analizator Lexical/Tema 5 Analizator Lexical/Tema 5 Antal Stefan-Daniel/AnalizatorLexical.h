#pragma once
#include<iostream>
#include<vector>
#include<string>
#include<fstream>
#include<sstream>
class AnalizatorLexical
{
public:
	void CitesteDateAnalizator();
	void SeparaUnitatliLexicale();
	void AnalizeazaUnitatileLExicale();
	void AfiseazaDateAnalizator();

private:
	bool IsString(const std::string& string);
	bool Comentariu(const std::string& string);
	bool Identificator(const std::string& string);
	bool Numar(const std::string& string);
	bool IsBool(const std::string& string);
	bool CuvantCheie(const std::string& string);
	bool Separator(const std::string& string);
	bool Spatiu(const std::string& str);
	bool Instructiune(const std::string& string);
	bool Operator(const std::string& string);

private:
	std::vector<std::string>m_cuvinteCheie;
	std::vector<std::string>m_operatori;
	std::vector<std::string>m_separatori;
	std::vector<std::string>m_instructiuni;
	std::vector<std::string>m_identificatori;
	std::vector<std::string>m_unitatiLexicale;

};


