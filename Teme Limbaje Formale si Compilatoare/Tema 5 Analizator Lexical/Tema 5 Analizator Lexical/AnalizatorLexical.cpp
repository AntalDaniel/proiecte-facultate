#include "AnalizatorLexical.h"

void AnalizatorLexical::CitesteDateAnalizator()
{
	std::ifstream fileInput("Input.txt");
	std::string str, word;
	std::stringstream line;
	if (fileInput.is_open())
	{
		std::getline(fileInput, str);
		line << str;
		while (line >> word)
		{
			m_cuvinteCheie.push_back(word);
		}
		line.clear();

		std::getline(fileInput, str);
		line << str;
		while (line >> word)
		{
			m_instructiuni.push_back(word);
		}
		line.clear();


		std::getline(fileInput, str);
		line << str;
		while (line >> word)
		{
			m_operatori.push_back(word);
		}
		line.clear();

		std::getline(fileInput, str);
		line << str;
		while (line >> word)
		{
			m_separatori.push_back(word);
		}
		line.clear();

		std::getline(fileInput, str);
		line << str;
		while (line >> word)
		{
			m_identificatori.push_back(word);
		}
		line.clear();
		fileInput.close();
	}
	else
		std::cout << "Nu s-a putut deschide fisierul!";

}

void AnalizatorLexical::SeparaUnitatliLexicale()
{
	std::ifstream fileInput("FisierIntrare.txt");
	if (fileInput.is_open())
	{
		bool comSimplu = false;
		bool comMultiplu = false;
		char caracter;
		std::string unitateLexicala;

		while (fileInput >> std::noskipws >> caracter)
		{
			std::string caracterToString;
			caracterToString.push_back(caracter);
			if (comSimplu || comMultiplu)
			{
				while (comSimplu && !fileInput.eof())
				{
					fileInput >> std::noskipws >> caracter;
					if (caracter == '\n')
						comSimplu = false;
				}
				while (comMultiplu && !fileInput.eof())
				{
					fileInput >> std::noskipws >> caracter;
					if (caracter == '*')
					{
						std::string aux;
						aux.push_back(caracter);
						fileInput >> std::noskipws >> caracter;
						if (caracter == '/')
						{
							comMultiplu = false;
							aux.push_back(caracter);
							m_unitatiLexicale.push_back(aux);
						}
					}

				}
				continue;
			}

			if (caracter == '/')
			{
				std::string operatorComentariu;
				operatorComentariu.push_back(caracter);
				fileInput >> std::noskipws >> caracter;

				if (caracter == '*')
				{
					comMultiplu = true;
					operatorComentariu += caracter;
					m_unitatiLexicale.push_back(operatorComentariu);
					continue;
				}
				else
					if (caracter == '/')
					{
						comSimplu = true;
						operatorComentariu += caracter;
						m_unitatiLexicale.push_back(operatorComentariu);
						continue;
					}
			}

			if (Spatiu(caracterToString))
			{
				if (!unitateLexicala.empty())
				{
					m_unitatiLexicale.push_back(unitateLexicala);
					unitateLexicala.clear();
				}
				continue;
			}

			if (Operator(caracterToString))
			{
				if (!unitateLexicala.empty() && !Operator(unitateLexicala))
				{
					m_unitatiLexicale.push_back(unitateLexicala);
					unitateLexicala.clear();
				}
			}

			if (Separator(caracterToString))
			{
				if (!unitateLexicala.empty())
				{
					m_unitatiLexicale.push_back(unitateLexicala);
					unitateLexicala.clear();
				}
				if (Separator(caracterToString))
				{
					std::string aux;
					aux.push_back(caracter);
					m_unitatiLexicale.push_back(aux);
					continue;
				}

			}
			unitateLexicala.push_back(caracter);
		}
		fileInput.close();
	}
	else
		std::cout << "Nu s-a putut deschide fisierul!";
}
void AnalizatorLexical::AnalizeazaUnitatileLExicale()
{
	for (auto unitateLexicala : m_unitatiLexicale)
	{
		if (IsBool(unitateLexicala))
			std::cout << "[ " << unitateLexicala << " -->Valoare booleana.]\n";
		else

			if (Identificator(unitateLexicala))
				std::cout << "[ " << unitateLexicala << " -->Identificator.]\n";
			else
				if (Numar(unitateLexicala))
					std::cout << "[ " << unitateLexicala << " -->Numar.]\n";
				else
					if (IsString(unitateLexicala))
						std::cout << "[ " << unitateLexicala << " -->String.]\n";
					else
						if (Comentariu(unitateLexicala))
							std::cout << "[ " << unitateLexicala << " -->Comentariu.]\n";
						else
							if (CuvantCheie(unitateLexicala))
								std::cout << "[ " << unitateLexicala << " -->Cuvant cheie.]\n";
							else
								if (Instructiune(unitateLexicala))
									std::cout << "[ " << unitateLexicala << " -->Instructiune.]\n";
								else
									if (Operator(unitateLexicala))
										std::cout << "[ " << unitateLexicala << " -->Operator.]\n";
									else
										if (Separator(unitateLexicala))
											std::cout << "[ " << unitateLexicala << " -->Separator.]\n";
										else
											std::cout << "[ " << unitateLexicala << " -->Mesaj Eroare.]\n";

	}
}
void AnalizatorLexical::AfiseazaDateAnalizator()
{
	std::cout << "Datele introduse in analizator sunt: \n";
	std::cout << "Cuvinte cheie= { ";

	for (auto index : m_cuvinteCheie)
		std::cout << index << " ";
	std::cout << " }\n";
	std::cout << "Operatori= { ";
	for (auto index : m_operatori)
		std::cout << index << " ";
	std::cout << " }\n";
	std::cout << "Instructiuni= { ";
	for (auto index : m_instructiuni)
		std::cout << index << " ";
	std::cout << " }\n";
	std::cout << "Separatori={ ";
	for(auto index: m_separatori)
		std::cout << index << " ";
	std::cout << " }\n";
	std::cout << "Identificatori={ ";
	for (auto index : m_identificatori)
		std::cout << index << " ";
	std::cout << " }\n";
	std::cout << std::endl;
	std::cout << std::endl;


}
bool AnalizatorLexical::Identificator(const std::string& string)
{
	if (std::find(m_identificatori.begin(), m_identificatori.end(), string) != m_identificatori.end())
		return true;
	return false;
}

bool AnalizatorLexical::Numar(const std::string& string)
{
	for (auto character : string)
	{
		if (isdigit(character) == false)
			return false;
	}
	return true;
}

bool AnalizatorLexical::IsBool(const std::string& string)
{
	if ((string == "true") || (string == "false"))
		return true;
	return false;
}

bool AnalizatorLexical::IsString(const std::string& string)
{
	if (string[0] == '"' && string[string.size() - 1] == '"')
		return true;
	return false;
}

bool AnalizatorLexical::Comentariu(const std::string& string)
{
	if (string == "//" || string == "/*" || string == "*/")
		return true;
	return false;
}


bool AnalizatorLexical::CuvantCheie(const std::string& string)
{
	if (std::find(m_cuvinteCheie.begin(), m_cuvinteCheie.end(), string) != m_cuvinteCheie.end())
		return true;
	return false;
}

bool AnalizatorLexical::Instructiune(const std::string& string)
{
	if (std::find(m_instructiuni.begin(), m_instructiuni.end(), string) != m_instructiuni.end())
		return true;
	return false;
}

bool AnalizatorLexical::Operator(const std::string& string)
{
	if (std::find(m_operatori.begin(), m_operatori.end(), string) != m_operatori.end())
		return true;
	return false;
}

bool AnalizatorLexical::Separator(const std::string& string)
{
	if (std::find(m_separatori.begin(), m_separatori.end(), string) != m_separatori.end())
		return true;
	return false;

}

bool AnalizatorLexical::Spatiu(const std::string& str)
{
	if ((str == " ") || (str == "\t") || (str == "\n"))
		return true;
	return false;
}
