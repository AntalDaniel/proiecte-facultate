#include "MinimizareAFD.h"

MinimizareAFD::MinimizareAFD()
{
	m_afd.Citire();
	m_afd.Afisare();
	m_afdMinimizat.SetSigma(m_afd.GetSigma());
	m_afd.EliminareStariInaccesibile();
	std::string aux(m_afd.GetStari().size(), ' ');
	std::vector<std::string> matrix(m_afd.GetStari().size(), aux);
	m_matrix = matrix;
	for (int index = 0; index < m_matrix.size(); index++)
	{
		m_matrix[index][index] = m_afd.GetStari()[index].back();
		for (int indexCol = 0; indexCol < index; indexCol++)
		{
			m_matrix[index][indexCol] = '-';
		}
	}
	std::cout << "Iteratia 0: Matricea initiala este: \n";
	AfisareMatrice();

}

void MinimizareAFD::AfisareMatrice()
{
	for (auto linie : m_matrix)
	{
		for (auto element : linie)
		{
			std::cout << element << " ";
		}
		std::cout << std::endl;
	}

}

void MinimizareAFD::MarcheazaFinale()
{
	auto stari = m_afd.GetStari();
	for (int indexLine = 0; indexLine < m_matrix.size(); indexLine++)
	{
		for (int indexCol = 0; indexCol < indexLine; indexCol++)
		{
			if ((stari[indexLine] != stari[indexCol]))
			{
				if (((m_afd.verificareFinal(stari[indexLine])) && (!m_afd.verificareFinal(stari[indexCol]))) || ((!m_afd.verificareFinal(stari[indexLine])) && (m_afd.verificareFinal(stari[indexCol]))))
				{
					m_matrix[indexLine][indexCol] = '#';
				}
			}
		}
	}
}

void MinimizareAFD::MarcheazaMatrice()
{
	auto stari = m_afd.GetStari();
	std::vector<std::string>matriceAuxiliara = m_matrix;
	std::unordered_map<std::string, std::string>delta = m_afd.GetDelta();
	int index1, index2;
	int numarIteratii = 0;
	bool schimbat = false;
	do {
		schimbat = false;
		for (int indexLine = 0; indexLine < m_matrix.size(); indexLine++)
		{
			for (int indexCol = 0; indexCol < indexLine; indexCol++)
			{
				if (m_matrix[indexLine][indexCol] == '-')
				{
					for (auto simbol : m_afd.GetSigma())
					{
						std::string cheie1, cheie2;
						cheie1 = stari[indexLine] + simbol;
						cheie2 = stari[indexCol] + simbol;
						if (delta.find(cheie1) != delta.end())
						{
							if (std::find(stari.begin(), stari.end(), delta.at(cheie1)) != stari.end())
							{
								index1 = std::distance(stari.begin(), std::find(stari.begin(), stari.end(), delta.at(cheie1)));
							}
						}
						if (delta.find(cheie2) != delta.end())
						{
							if (std::find(stari.begin(), stari.end(), delta.at(cheie2)) != stari.end())
							{
								index2 = std::distance(stari.begin(), std::find(stari.begin(), stari.end(), delta.at(cheie2)));
							}
						}
						if (index1 != index2)
						{
							if (index1 < index2)
								std::swap(index1, index2);
							if (matriceAuxiliara[index1][index2] != '-')
							{
								matriceAuxiliara[indexLine][indexCol] = '#';
								schimbat = true;
							}
						}
					}
				}
			}
		}
		numarIteratii++;
		std::cout << "Iteratia " << numarIteratii << std::endl;
		AfisareMatrice();
		m_matrix = matriceAuxiliara;
	} while (schimbat == true);
}

bool MinimizareAFD::EsteEchivalenta(const std::string& stare)
{
	for (auto elem : m_stariEchiv)
	{
		if (std::find(elem.begin(), elem.end(), stare) != elem.end())
			return true;
	}
	return false;
}

void MinimizareAFD::PopulareStariEchivalente()
{
	std::vector<std::string> stari = m_afd.GetStari();
	for (int indexColoana = 0; indexColoana < m_matrix.size(); indexColoana++)
	{
		std::vector<std::string>stariEchivalente;
		stariEchivalente.push_back(stari[indexColoana]);
		if (!EsteEchivalenta(stari[indexColoana]))
		{
			for (int indexLinie = indexColoana + 1; indexLinie < m_matrix.size(); indexLinie++)
			{
				if (m_matrix[indexLinie][indexColoana] == '-')
					stariEchivalente.push_back(stari[indexLinie]);
			}
			m_stariEchiv.push_back(stariEchivalente);
		}
	}
	
}

void MinimizareAFD::AfiseazaStariEchiv()
{
	std::cout << "\n Pentru verificare!\n";
	std::cout << "Starile Echivalente sunt: \n";
	for (auto stariEchiv : m_stariEchiv)
	{
		for (auto stare : stariEchiv)
		{
			std::cout << stare << " ";
		}
		std::cout << std::endl;
	}


}

void MinimizareAFD::RedenumireStari()
{
	int numarStare = 0;
	std::vector<std::string> stariRedenumite;
	std::string stareInitiala;
	std::vector<std::string>stariFinale;
	for (auto stariEchivalente : m_stariEchiv)
	{
		std::string stare = stariEchivalente.front();
		if (stare == m_afd.GetStareInitiala())
			stareInitiala = "q" + std::to_string(numarStare);
		for (auto index : stariEchivalente)
		{
			if ((m_afd.verificareFinal(index))&&(std::find(stariFinale.begin(),stariFinale.end(), "q" + std::to_string(numarStare))==stariFinale.end()))
			{
				stariFinale.push_back("q" + std::to_string(numarStare));
			}
			m_stariRedenumite.insert(std::make_pair(index, "q" + std::to_string(numarStare)));
		}
		stariRedenumite.push_back("q" + std::to_string(numarStare));
		numarStare++;
	}
	m_afdMinimizat.SetStari(stariRedenumite);
	m_afdMinimizat.SetFinale(stariFinale);
	m_afdMinimizat.SetStareInitiala(stareInitiala);
}

void MinimizareAFD::ConstruireTranzitii()
{
	std::unordered_map<std::string, std::string> deltaMinimizat;
	std::unordered_map<std::string, std::string> deltaInitial=m_afd.GetDelta();
	auto stari = m_afdMinimizat.GetStari();
	int index = 0;
	for (auto stare : m_stariEchiv)
	{
		for (auto simbol : m_afd.GetSigma())
		{
			auto rezultat = deltaInitial.at(stare.front() + simbol);
			std::string cheie = stari[index]+simbol;
			// posibil nevoie sa verific daca se afla in map deja inainte de insert
			deltaMinimizat.insert(std::make_pair(cheie, m_stariRedenumite.at(rezultat)));
		}
			index++;
	}
	m_afdMinimizat.SetDelta(deltaMinimizat);

}

void MinimizareAFD::Minimizare()
{
	MarcheazaFinale();
	MarcheazaMatrice();
	PopulareStariEchivalente();
	//test
	AfiseazaStariEchiv();
	RedenumireStari();
	ConstruireTranzitii();
}

AFD MinimizareAFD::GetAfdMinimizat()
{
	return m_afdMinimizat;
}


