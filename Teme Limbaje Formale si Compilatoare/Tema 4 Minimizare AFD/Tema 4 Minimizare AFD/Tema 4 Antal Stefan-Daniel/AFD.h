#pragma once
#include<iostream>
#include<vector>
#include<string>
#include<unordered_map>
#include<fstream>
#include<queue>
class AFD
{
public:
	void Citire();
	void Afisare();
	bool VerificareCuvant(const std::string& cuvant);
	std::vector<std::string>GetStari();
	std::vector<std::string>GetFinale();
	std::string GetSigma();
	std::string GetStareInitiala();
	std::unordered_map<std::string, std::string>GetDelta();
	void SetStari(std::vector<std::string> stari);
	void SetFinale(std::vector<std::string> finale);
	void SetStareInitiala(std::string start);
	void SetDelta(std::unordered_map<std::string, std::string> delta);
	void SetSigma(std::string sigma);
	bool verificareFinal(const std::string stare);
	void EliminareStariInaccesibile();



private:
	std::vector<std::string> m_stari;
	std::string m_sigma;
	std::unordered_map<std::string, std::string> m_delta;
	std::string m_stareInitiala;
	std::vector<std::string> m_finale;
};

