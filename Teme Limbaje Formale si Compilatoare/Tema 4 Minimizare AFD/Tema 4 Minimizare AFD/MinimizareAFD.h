#pragma once
#include"AFD.h"
class MinimizareAFD
{ public:
	MinimizareAFD();
	void AfisareMatrice();
	void MarcheazaFinale();
	void MarcheazaMatrice();
	bool EsteEchivalenta(const std::string& stare);
	void PopulareStariEchivalente();
	void AfiseazaStariEchiv();
	void RedenumireStari();
	void ConstruireTranzitii();
	void Minimizare();
	AFD GetAfdMinimizat();
	
	
private:
	AFD m_afd;
	AFD m_afdMinimizat;
	std::vector<std::string> m_matrix;
	std::vector<std::vector<std::string>>m_stariEchiv;
	std::unordered_map<std::string, std::string>m_stariRedenumite;

};

