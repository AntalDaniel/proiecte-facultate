#include "AFD.h"
void AFD::Citire()
{
	std::ifstream fileInput("Input.txt");
	if (fileInput.is_open())
	{
		int numberOfState;
		int numberOfFinalStates;
		std::string aux;
		std::string index;
		fileInput >> numberOfState;
		for (int index = 0; index < numberOfState; index++)
		{
			fileInput >> aux;
			m_stari.push_back(aux);
		}
		fileInput >> m_sigma;
		fileInput >> m_stareInitiala;
		fileInput >> numberOfFinalStates;
		for (int index = 0; index < numberOfFinalStates; index++)
		{
			fileInput >> aux;
			m_finale.push_back(aux);
		}
		while (!fileInput.eof())
		{
			fileInput >> index;
			fileInput >> aux;
			m_delta.insert(make_pair(index, aux));
		}


	}
	else
		std::cout << "Nu s-a putut deschide fisierul!\n";
	fileInput.close();
}

void AFD::Afisare()
{
	std::cout << "Automatul este: \n";
	std::cout << "Stari={";
	for (auto stare : m_stari)
	{
		std::cout << stare << ", ";
	}
	std::cout << "}\n";
	std::cout << "Sigma={" << m_sigma << "}\n";
	std::cout << "Stare Initiala={" << m_stareInitiala << "}\n";
	std::cout << "Stari finale={";
	for (auto stareFinala : m_finale)
	{
		std::cout << stareFinala << ", ";
	}
	std::cout << "}\n";
	std::cout << "Delta: \n";
	for (auto tranzition : m_delta)
	{
		std::string aux = tranzition.first;
		char chr = aux.back();
		aux.pop_back();
		std::cout << aux << ' ' << chr << ' ' << tranzition.second << std::endl;
	}
}

bool AFD::VerificareCuvant(const std::string& cuvant)
{
	std::string index = m_stareInitiala;
	std::string aux;
	bool isWordOK = false;
	for (auto character : cuvant)
	{
		index.push_back(character);
		if (m_delta.find(index) == m_delta.end())
			throw "Cuvantul a ajuns la blocaj!\n";
		else
		{
			aux = m_delta[index];
			index.clear();
			index = aux;
		}
	}
	for (auto finalState : m_finale)
	{
		if (finalState.find(index) != std::string::npos)
			isWordOK = true;

	}
	if (isWordOK == false)
		throw "Cuvantul nu este acceptat!\n";
	return isWordOK;
}




std::vector<std::string> AFD::GetStari()
{
	return m_stari;
}

std::vector<std::string> AFD::GetFinale()
{
	return m_finale;
}

std::string AFD::GetSigma()
{
	return m_sigma;
}

std::string AFD::GetStareInitiala()
{
	return m_stareInitiala;
}

std::unordered_map<std::string, std::string> AFD::GetDelta()
{
	return m_delta;
}
void AFD::SetStari(std::vector<std::string> stari)
{
	m_stari = stari;
}
void AFD::SetFinale(std::vector<std::string> finale)
{
	m_finale = finale;
}
void AFD::SetStareInitiala(std::string start)
{
	m_stareInitiala = start;
}
void AFD::SetDelta(std::unordered_map<std::string, std::string> delta)
{
	m_delta = delta;
}
void AFD::SetSigma(std::string sigma)
{
	m_sigma = sigma;
}
bool AFD::verificareFinal(const std::string stare)
{
	if (std::find(m_finale.begin(), m_finale.end(), stare) != m_finale.end())
	{
		return true;
	}
	return false;
}
void AFD::EliminareStariInaccesibile()
{
	std::vector<std::string> stariVizitate;
	std::queue<std::string> coada;
	coada.push(m_stareInitiala);
	while (!coada.empty())
	{
		std::string cheie = coada.front();
		std::string rezultat;
		for (char simbol : m_sigma)
		{
			rezultat = m_delta.at(cheie + simbol);
			if (std::find(stariVizitate.begin(), stariVizitate.end(), rezultat) == stariVizitate.end())
			{
				coada.push(rezultat);
			}
		}
		if (std::find(stariVizitate.begin(), stariVizitate.end(), cheie) == stariVizitate.end())
		{
			stariVizitate.push_back(cheie);
		}
		coada.pop();
	}
	//m_stari = stariVizitate;
	//SAU PENTRU O VERIFICARE MAI USOARA, STARILE RAMAN ORDONATE
	for (int index=0;index< m_stari.size();index++)
	{
		if (std::find(stariVizitate.begin(), stariVizitate.end(), m_stari[index]) == stariVizitate.end())
		{
			m_stari.erase(m_stari.begin()+index);
		}
	}

}

