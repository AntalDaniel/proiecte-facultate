#include"AFD.h"
#include"MinimizareAFD.h"
int main()
{
	
	std::cout << "Automatul initial este: ";
	MinimizareAFD afdMinimizat;
	afdMinimizat.Minimizare();
	afdMinimizat.GetAfdMinimizat().Afisare();
	std::cout << "\n########## Minimizare efectuata cu succes!!!! ##########\n";

	bool isCorrect;
	uint16_t numberOfWords;
	std::vector<std::string> listOfWords;
	std::string cuvant;
	uint16_t option;
	do
	{
		std::cout << "\n";
		std::cout << "Alegeti optiunea pe care doriti sa o afisati!\n";
		std::cout << "1.Afiseaza afiseaza automatul.\n";
		std::cout << "2.Verifica daca un cuvant citi de la tastatura este corect.\n";
		std::cout << "3.Verifica daca un sir de cuvinte citite de la tastatra sunt corecte.\n";
		std::cout << "4.Exit.\n";
		std::cout << "Introduceti optiunea:";
		std::cin >> option;
		std::cout << std::endl;
		switch (option)
		{
		case 1:
			std::cout << "\n";
			afdMinimizat.GetAfdMinimizat().Afisare();
			break;
		case 2:
			std::cout << "\n";
			std::cout << "Introduceti cuvantul pe care doriti sa il verificati: ";
			std::cin >> cuvant;
			try
			{
				isCorrect = afdMinimizat.GetAfdMinimizat().VerificareCuvant(cuvant);
				if (isCorrect)
					std::cout << "Cuvantul: " << cuvant << " este acceptat de automat.\n";
			}
			catch (const char* mesajEroare)
			{
				std::cout << cuvant << "==>" << mesajEroare;
			}
			break;
		case 3:
			std::cout << "\n";
			std::cout << "Introduceti numarul cuvintelor pe care doriti sa le verificati: ";
			std::cin >> numberOfWords;
			std::cout << "Introduceti cuvintele pe care doriti sa le verificati: ";
			for (int index = 0; index < numberOfWords; index++)
			{
				std::cin >> cuvant;
				listOfWords.push_back(cuvant);
			}
			std::cout << "\n";
			for (auto word : listOfWords)
			{
				isCorrect = false;
				try
				{

					isCorrect = afdMinimizat.GetAfdMinimizat().VerificareCuvant(word);
					if (isCorrect)
						std::cout << "Cuvantul: " << word << " este acceptat de automat.\n";

				}
				catch (const char* mesajEroare)
				{
					std::cout << word << "==>" << mesajEroare;
				}
			}
			break;
		case 4:
			std::cout << "Programul s-a inchis.\n";
			break;
		default:
			std::cout << "Optiunea introdusa este gresita!!!\n";
			std::cout << "Introduceti  optiunea 0  pentru a restarta meniul.";
			std::cin >> option;
			std::cout << "\n";
			break;
		}
	} while (option != 4);
	return 0;


}