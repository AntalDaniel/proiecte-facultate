#include "AFN.h"
void AFN::Citire()
{
	std::ifstream fileInput("Input.txt");
	if (fileInput.is_open())
	{
		int numberOfState;
		int numberOfFinalStates;
		std::string aux;
		std::string index;
		fileInput >> numberOfState;
		for (int index = 0; index < numberOfState; index++)
		{
			fileInput >> aux;
			m_stari.push_back(aux);
		}
		fileInput >> m_sigma;
		fileInput >> m_stareInitiala;
		fileInput >> numberOfFinalStates;
		for (int index = 0; index < numberOfFinalStates; index++)
		{
			fileInput >> aux;
			m_finale.push_back(aux);
		}
		while (!fileInput.eof())
		{
			fileInput >> index;
			fileInput >> aux;
			m_delta.insert(make_pair(index, aux));
		}


	}
	else
		std::cout << "Nu s-a putut deschide fisierul!\n";
	fileInput.close();
}

void AFN::Afisare()
{
	std::cout << "Automatul este: \n";
	std::cout << "Stari={";
	for (auto stare : m_stari)
	{
		std::cout << stare << ", ";
	}
	std::cout << "}\n";
	std::cout << "Sigma={" << m_sigma << "}\n";
	std::cout << "Stare Initiala={" << m_stareInitiala << "}\n";
	std::cout << "Stari finale={";
	for (auto stareFinala : m_finale)
	{
		std::cout << stareFinala << ", ";
	}
	std::cout << "}\n";
	std::cout << "Delta: \n";
	std::unordered_map<std::string, std::string> newTranzition;
	for (auto tranzition : m_delta)
	{
		std::string aux = tranzition.second+", ";
		newTranzition[tranzition.first] = newTranzition[tranzition.first] + aux;
	}
	for (auto elem : newTranzition)
	{	std::string aux = elem.first;
		char chr = aux.back();
		aux.pop_back();
		std::cout << aux << "(" << chr << ") ={ " << elem.second <<"}\n";
	}
}

bool AFN::VerificareCuvant(const std::string& cuvant)
{
	std::vector<std::string>newListOfIndexs;
	std::vector<std::string> listOfIndexs;
	std::string cuvantAuxiliar;
	bool isWordOK = false;

	cuvantAuxiliar.assign(cuvant.begin() + 1, cuvant.end());
	auto range = m_delta.equal_range(m_stareInitiala + cuvant[0]);

	for (auto iterator = range.first; iterator != range.second; ++iterator)
	{
		listOfIndexs.push_back(iterator->second);
	}

	for (auto character : cuvantAuxiliar)
	{
		for (auto index : listOfIndexs)
		{
			index.push_back(character);
			auto range = m_delta.equal_range(index);
			for (auto iterator = range.first; iterator != range.second; ++iterator)
			{
				newListOfIndexs.push_back(iterator->second);
			}
		}

		listOfIndexs = newListOfIndexs;
		newListOfIndexs.clear();
	}

	if (listOfIndexs.size() != 0)
	{
		for (auto index : listOfIndexs)
		{
			if (std::find(m_finale.begin(), m_finale.end(), index) != m_finale.end())
				isWordOK = true;

		}
		if (isWordOK == false)
			throw "Cuvantul nu este acceptat!\n";

	}
	else
		throw "Blocaj\n";

	return isWordOK;
}

