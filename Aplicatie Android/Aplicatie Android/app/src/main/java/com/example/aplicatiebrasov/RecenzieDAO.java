package com.example.aplicatiebrasov;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface RecenzieDAO {

    @Insert
    public void insertRecenzie(Recenzie recenzie);

    @Query("select * from Recenzii where idRest = :idRest")
    public List<Recenzie> selectRecenziiByID(int idRest);

    @Query("select avg(stars) from Recenzii where idRest = :idRest;")
    public float selectAvgStarsByID(int idRest);

    @Query("delete from Recenzii where idRest = :idRest;")
    public void deleteRecenzieById(int idRest);
}
