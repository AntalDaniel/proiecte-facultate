package com.example.aplicatiebrasov;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface MesajDAO {
    @Insert
    public void insertMesaj(Mesaj mesaj);

    @Query("select * from Mesaje;")
    public List<Mesaj> selectAllMesaje();

    @Query("delete from Mesaje;")
    public void deleteAllMesaje();

    @Query("delete from Mesaje where id = :id;")
    public void deleteMesajById(int id);
}
