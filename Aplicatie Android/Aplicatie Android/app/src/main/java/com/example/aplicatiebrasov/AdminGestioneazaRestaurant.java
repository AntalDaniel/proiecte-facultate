package com.example.aplicatiebrasov;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AdminGestioneazaRestaurant extends AppCompatActivity {

    private AppDatabase database = null;
    boolean isEdit;
    EditText idET;
    EditText numeET;
    EditText addrET;
    EditText telET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_gestioneaza_restaurant);

        database = Room.databaseBuilder(this, AppDatabase.class, "AppDatabase").allowMainThreadQueries().fallbackToDestructiveMigration().build();

        Intent it = getIntent();
        isEdit = it.getBooleanExtra("isEdit", false);

        idET = findViewById(R.id.idRestET);
        numeET = findViewById(R.id.numeRestET);
        addrET = findViewById(R.id.adresaRestET);
        telET = findViewById(R.id.telefonRestET);

        if(isEdit){
            Restaurant r = database.restaurantDAO().selectRestaurantByID(it.getIntExtra("idRest", -1));
            idET.setText(Integer.toString(r.getId()));
            idET.setEnabled(false);
            numeET.setText(r.getNume());
            addrET.setText(r.getAdresa());
            telET.setText(r.getTelRezervari());
        }else{
            findViewById(R.id.deleteBTN).setVisibility(View.INVISIBLE);
        }
    }

    public void salveazaRestaurant(View view) {
        int id = Integer.valueOf(idET.getText().toString());
        String nume = numeET.getText().toString();
        String addr = addrET.getText().toString();
        String tel = telET.getText().toString();

        if((idET.getText().toString().matches("")) || (nume.matches("")) || (addr.matches("")) || (tel.matches(""))){
            Toast.makeText(this, "Toate campurile sunt obligatorii!", Toast.LENGTH_SHORT).show();
        }else{
            if(isEdit){
                //update DB & return
                database.restaurantDAO().updateRestaurantById(id,nume,addr,tel);
            } else{
                //insert & return
                Restaurant r = new Restaurant(id, nume, addr, tel, 0.0f);
                database.restaurantDAO().insertRestaurant(r);
            }
            finish();
        }
    }

    public void deleteRestaurant(View view) {
        database.recenzieDAO().deleteRecenzieById(Integer.valueOf(idET.getText().toString()));
        database.restaurantDAO().deleteRestaurantById(Integer.valueOf(idET.getText().toString()));
        finish();
    }
}
