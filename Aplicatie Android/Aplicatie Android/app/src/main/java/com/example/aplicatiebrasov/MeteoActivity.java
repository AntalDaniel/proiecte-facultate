package com.example.aplicatiebrasov;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class MeteoActivity extends AppCompatActivity {

    public class Get5DayForecast extends AsyncTask<String, Void, ArrayList<PrognozaMeteo>>{

        @Override
        protected ArrayList<PrognozaMeteo> doInBackground(String... strings) {
            ArrayList<PrognozaMeteo> listJSON = new ArrayList<>();

            String link = strings[0];
            try {
                URL url = new URL(link);
                HttpURLConnection http = (HttpURLConnection) url.openConnection();
                InputStream is = http.getInputStream();

                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                String linie = null;
                StringBuilder builder = new StringBuilder();

                while((linie=reader.readLine())!=null){
                    builder.append(linie);
                }

                String text = builder.toString();

                JSONObject mainObject = new JSONObject(text);
                JSONArray fiveDayForecast = mainObject.getJSONArray("DailyForecasts");
                for(int i=0;i<5;i++){
                    JSONObject dailyWeather = fiveDayForecast.getJSONObject(i);
                    String data = (dailyWeather.getString("Date")).substring(0,10);
                    JSONObject temperature = dailyWeather.getJSONObject("Temperature");
                    JSONObject maximum = temperature.getJSONObject("Maximum");
                    float temp = (float) maximum.getDouble("Value");
                    JSONObject day = dailyWeather.getJSONObject("Day");
                    String desc = day.getString("IconPhrase");

                    PrognozaMeteo p = new PrognozaMeteo(data, temp, desc);
                    listJSON.add(p);
                }

            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }

            return listJSON;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meteo);

        Get5DayForecast prognoza = new Get5DayForecast(){
            @Override
            protected void onPostExecute(ArrayList<PrognozaMeteo> prognozaMeteos) {
                AdaptorPrognozaMeteo adaptor = new AdaptorPrognozaMeteo(getApplicationContext(), R.layout.prognozameteo_item_layout, prognozaMeteos);
                ((ListView)findViewById(R.id.prognozameteo_lista_LV)).setAdapter(adaptor);
            }
        };
        prognoza.execute("http://dataservice.accuweather.com/forecasts/v1/daily/5day/287345?apikey=rxEOHhLyDEAcwAZDTtFvFekw4TJWkvWB&language=ro&metric=true");
    }
}
