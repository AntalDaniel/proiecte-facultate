package com.example.aplicatiebrasov;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ObiectiveTuristiceActivity extends AppCompatActivity {

    private ArrayList<ObiectivTuristic> listaObiective;
    private ArrayList<String> denumiriObiective;
    private int requestCode = 101;

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference databaseObiective = database.getReference();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_obiective_turistice);

        listaObiective = new ArrayList<>();
        denumiriObiective = new ArrayList<>();

        final ListView lv = findViewById(R.id.obiectiveLV);

        databaseObiective.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listaObiective.clear();

                DataSnapshot obiectiveRef = dataSnapshot.child("obiective");
                Iterable<DataSnapshot> snapshots = obiectiveRef.getChildren();

                for(DataSnapshot snapshot : snapshots){
                    ObiectivTuristic obiectiv = snapshot.getValue(ObiectivTuristic.class);

                    listaObiective.add(obiectiv);
                    denumiriObiective.add(obiectiv.getNume());

                    ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplication(), android.R.layout.simple_list_item_1, denumiriObiective);
                    lv.setAdapter(adapter);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), ObiectivDetaliatAtcivity.class);
                intent.putExtra("obiectiv", listaObiective.get(position));
                startActivityForResult(intent, requestCode);
            }
        });
    }
}
