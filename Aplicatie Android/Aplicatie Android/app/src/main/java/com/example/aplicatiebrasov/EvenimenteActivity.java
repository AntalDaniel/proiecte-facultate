package com.example.aplicatiebrasov;

import androidx.annotation.ArrayRes;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class EvenimenteActivity extends AppCompatActivity {

    private ArrayList<Eveniment> listaEvenimente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evenimente);

        listaEvenimente = new ArrayList<>();
        listaEvenimente.add(new Eveniment("Interact Football Cup 2019", "Interact Club Brașov revine cu cea de a doua ediție a cupei de fotbal ”Football Cup”, cu același scop ca și la “Streetball Challenge”, promovarea mișcării și a sportului Brașovean.\n" +
                "\n" +
                "Meciurile se vor desfășura sub formula 6v6 (5 jucători + 1 portar). Fiecare echipă are dreptul la o rezervă, și maxim doi jucători legitimați și activi. (Este obligatoriu să prezentați buletinul pentru a confrima că nu sunteți legitimați)"));
        listaEvenimente.add(new Eveniment("Festivalul National 'Brasov Art'", "FESTIVALUL – CONCURS DE INTERPRETARE VOCALĂ \"BRAȘOV ART TALENT”, Ediţia a V-a, Brasov 9 - 10 noiembrie 2019"));
        listaEvenimente.add(new Eveniment("Stand Up Comedy", "Pe 10 noiembrie, la ora 21:00, vino în Social Pub-Lounge Brașov pentru un show de Stand-Up Comedy cu George Bonea, Daniel Hărmănescu și Alex Cazacu. Niciunul nu este finalist iUmor, probabil pentru că niciunul nu a fost la iUmor.\n" +
                "\n" +
                "Mama lui Daniel Hărmănescu și-a dorit să se facă preot, dar nu i-a plăcut cum îi venea sutana așa că s-a apucat acum mulți ani de stand-up, fiind printre cei mai vechi din țară."));
        listaEvenimente.add(new Eveniment("Expozitia Ex-Libris Brancusi", "Expoziția „Ex-Libris Brâncuși” este parte integrantă a simpozionului ce poartă numele renumitului artist, manifestare ce se desfăşoară atât pe teritoriul ţării noastre, cât şi în străinătate, pe durata unui deceniu, începând din anul 2016.\n" +
                "Proiectul este derulat de Uniunea Artiştilor Plastici din România, Universitatea Naţională de Arte din Bucureşti, Universitatea de Vest din Timişoara - Facultatea de Arte şi Design şi Universitatea \"Ovidius\" din Constanţa - Facultatea de Arte, sub patronajul Comisiei Naţionale a României pentru UNESCO, reunind lucrările a peste 200 de artişti plastici (graficieni, pictori, sculptori, ceramişti etc.) din România, Bulgaria, Republica Moldova, Ungaria, Serbia și Turcia, dar şi alte personalități culturale. "));
        listaEvenimente.add(new Eveniment("Filmul \"Cărturan\"", "Viaţa lui Cărturan, un bărbat de 60 de ani care trăieşte într-o comună, este dată peste cap când află că, din cauza unei boli incurabile despre care nu ştia nimic, mai are de trăit foarte puţin.\n" +
                "\n" +
                "Situaţia lui este cu atât mai complicată pentru că îl are în grijă pe nepotul său de 12 ani, pe care bărbatul îl creşte singur de când aceste era foarte mic- el fiind singura rudă a copilului.\n" +
                "\n" +
                "Obişnuit cu dificultăţile vieţii, Cărturan îşi începe, astfel, mica odisee de dinaintea morţii."));

        final Spinner spinner = findViewById(R.id.evenimenteSPN);
        final TextView tv = findViewById(R.id.descriereEvenimentTV);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                for (Eveniment e : listaEvenimente) {
//                    if(spinner.getSelectedItem().toString() == e.getNume()){
//                        tv.setText("Am resusit");
//                    }
//                }
                tv.setText(listaEvenimente.get(position).getDescriere());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}
