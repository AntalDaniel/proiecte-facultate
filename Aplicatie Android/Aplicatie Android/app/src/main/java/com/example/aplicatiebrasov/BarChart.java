package com.example.aplicatiebrasov;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;
import android.widget.Toast;
import java.util.List;

public class BarChart extends View {

    List<Restaurant> listaRestaurante = null;

    public BarChart(Context context, List<Restaurant> listaRestaurante) {
        super(context);
        this.listaRestaurante = listaRestaurante;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int nrRest = listaRestaurante.size();
        float width = 1000/nrRest;
        float avgMax = 0;
        for(Restaurant r : listaRestaurante){
            if(r.getAvgRating()>avgMax){
                avgMax = r.getAvgRating();
            }
        }
        float minY = 500;
        float distText = 70;
        Paint paint = new Paint();
        paint.setStrokeWidth(width/nrRest);
        paint.setTextSize(50);

        for(int i=0;i<listaRestaurante.size();i++){
            paint.setColor(Color.rgb((i+1)*150%256, (i+1)*60%256,(i+1)*300%256));
            canvas.drawRect(width*i+30, minY-((listaRestaurante.get(i).getAvgRating()/avgMax)*400),width*i+width+30, minY, paint);
            paint.setColor(Color.BLACK);
            canvas.drawText((i+1) + ". " + listaRestaurante.get(i).getNume() + " (" + listaRestaurante.get(i).getAvgRating() + ")", 30, minY+(distText*(i+1)), paint);
        }


    }
}
