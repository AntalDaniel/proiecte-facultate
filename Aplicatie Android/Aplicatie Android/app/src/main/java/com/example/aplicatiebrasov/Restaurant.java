package com.example.aplicatiebrasov;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import java.util.ArrayList;

@Entity(tableName = "Restaurante")
public class Restaurant implements Parcelable {

    @PrimaryKey(autoGenerate = false)
    private int id;
    private String nume;
    private String adresa;
    private String telRezervari;
    private float avgRating;

    public Restaurant(int id, String nume, String adresa, String telRezervari, float avgRating) {
        this.id = id;
        this.nume = nume;
        this.adresa = adresa;
        this.telRezervari = telRezervari;
        this.avgRating = avgRating;
    }

    public String getNume() {
        return nume;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getTelRezervari() {
        return telRezervari;
    }

    public void setTelRezervari(String telRezervari) {
        this.telRezervari = telRezervari;
    }

    public float getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(float avgRating) {
        this.avgRating = avgRating;
    }

    protected Restaurant(Parcel in) {
        nume = in.readString();
        adresa = in.readString();
        telRezervari = in.readString();
        avgRating = in.readFloat();
    }

    public static final Creator<Restaurant> CREATOR = new Creator<Restaurant>() {
        @Override
        public Restaurant createFromParcel(Parcel in) {
            return new Restaurant(in);
        }

        @Override
        public Restaurant[] newArray(int size) {
            return new Restaurant[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nume);
        dest.writeString(adresa);
        dest.writeString(telRezervari);
        dest.writeFloat(avgRating);
    }
}

