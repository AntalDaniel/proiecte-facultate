package com.example.aplicatiebrasov;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class AdminLoginActivity extends AppCompatActivity {

    private EditText username;
    private EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_admin_);

        SharedPreferences sp = getSharedPreferences("admin", MODE_PRIVATE);
        ((EditText)findViewById(R.id.usernameET)).setText(sp.getString("admin", ""));
        ((EditText)findViewById(R.id.passwordET)).setText(sp.getString("pass", ""));
        ((CheckBox)findViewById(R.id.saveLoginCB)).setChecked(sp.getBoolean("save", false));

        Toast.makeText(this, "User: admin, pass: admin", Toast.LENGTH_SHORT).show();
    }

    public void cancel(View view) {
        finish();
    }

    public void login(View view) {
        username = findViewById(R.id.usernameET);
        password = findViewById(R.id.passwordET);

        if((username.getText().toString().matches("admin")) && (password.getText().toString().matches("admin"))){
            CheckBox checkBox = findViewById(R.id.saveLoginCB);
            SharedPreferences sp;
            SharedPreferences.Editor editor;
            if(checkBox.isChecked()){
                sp = getSharedPreferences("admin", MODE_PRIVATE);
                editor = sp.edit();
                editor.putString("admin", username.getText().toString());
                editor.putString("pass", password.getText().toString());
                editor.putBoolean("save", true);
                editor.commit();
            }else{
                sp = getSharedPreferences("admin", MODE_PRIVATE);
                editor = sp.edit();
                editor.putString("admin", "");
                editor.putString("pass", "");
                editor.putBoolean("save", false);
                editor.commit();
            }

            Intent it = new Intent(this, AdminActivity.class);
            startActivity(it);
        } else {
            Toast.makeText(this, "Date de login gresite!", Toast.LENGTH_SHORT).show();
            password.setText("");
        }
    }
}
