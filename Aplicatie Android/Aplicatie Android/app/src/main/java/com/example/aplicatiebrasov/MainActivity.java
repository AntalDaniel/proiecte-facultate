package com.example.aplicatiebrasov;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {


    public void adminLogin(View view) {
        Intent it = new Intent(this, AdminLoginActivity.class);
        startActivity(it);
    }

    public class GetWeather extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            String result = null;

            String link = strings[0];
            try {
                URL url = new URL(link);
                HttpURLConnection http = (HttpURLConnection) url.openConnection();
                InputStream is = http.getInputStream();

                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                String linie = null;
                StringBuilder builder = new StringBuilder();

                while((linie=reader.readLine())!=null){
                    builder.append(linie);
                }

                String text = builder.toString();

                JSONArray jsonArray = new JSONArray(text);
                JSONObject obiect = jsonArray.getJSONObject(0);
                JSONObject temperatura = obiect.getJSONObject("Temperature");
                JSONObject metric = temperatura.getJSONObject("Metric");
                double nrGrade = metric.getDouble("Value");
                result = " " + nrGrade + " °C";

            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }

            return result;
        }
    }

    public class GetImage extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... strings) {
            Bitmap bitmap = null;

            String link = strings[0];
            URL url = null;
            try {
                url = new URL(link);
                bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());

            } catch (IOException e) {
                e.printStackTrace();
            }

            return bitmap;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        GetWeather json = new GetWeather(){
            @Override
            protected void onPostExecute(String s) {
                ((TextView)findViewById(R.id.vremeTV)).setText(s);
            }
        };
        json.execute("http://dataservice.accuweather.com/currentconditions/v1/287345?apikey=rxEOHhLyDEAcwAZDTtFvFekw4TJWkvWB");
        GetImage image = new GetImage(){
            @Override
            protected void onPostExecute(Bitmap bitmap) {
                ImageView imageView = findViewById(R.id.logoImgView);
                imageView.setImageBitmap(bitmap);
            }
        };
        image.execute("http://dice.ase.ro/wp-content/uploads/2018/03/dice-e1523018803374.png");
    }

    public void deschideObiectiveTuristice(View view) {
        Intent it = new Intent(this, ObiectiveTuristiceActivity.class);
        startActivity(it);
    }

    public void deschideEvenimente(View view) {
        Intent it = new Intent(this, EvenimenteActivity.class);
        startActivity(it);
    }

    public void deschideRestaurante(View view) {
        Intent it = new Intent(this, RestauranteActivity.class);
        startActivity(it);
    }

    public void deschideInfo(View view) {
        Intent it = new Intent(this, InfoActivity.class);
        startActivity(it);
    }

    public void deschideMeteo(View view) {
        Intent it = new Intent(this, MeteoActivity.class);
        startActivity(it);
    }
}
