package com.example.aplicatiebrasov;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(tableName = "Recenzii", foreignKeys = @ForeignKey(entity = Restaurant.class,
        parentColumns = "id",
        childColumns = "idRest",
        onDelete = ForeignKey.CASCADE))
public class Recenzie {
    @PrimaryKey(autoGenerate = true)
    private int recId;
    private int idRest;
    private float stars;
    private String mesaj;

    public Recenzie(int idRest, float stars, String mesaj) {
        this.idRest = idRest;
        this.stars = stars;
        this.mesaj = mesaj;
    }

    public int getRecId() {
        return recId;
    }

    public void setRecId(int recId) {
        this.recId = recId;
    }

    public int getIdRest() {
        return idRest;
    }

    public void setIdRest(int idRest) {
        this.idRest = idRest;
    }

    public float getStars() {
        return stars;
    }

    public void setStars(float stars) {
        this.stars = stars;
    }

    public String getMesaj() {
        return mesaj;
    }

    public void setMesaj(String mesaj) {
        this.mesaj = mesaj;
    }
}
