package com.example.aplicatiebrasov;

import android.os.Parcel;
import android.os.Parcelable;

public class PrognozaMeteo implements Parcelable {

    private String data;
    private float temperatura;
    private String descriere;

    public PrognozaMeteo(String data, float temperatura, String descriere) {
        this.data = data;
        this.temperatura = temperatura;
        this.descriere = descriere;
    }

    protected PrognozaMeteo(Parcel in) {
        data = in.readString();
        temperatura = in.readFloat();
        descriere = in.readString();
    }

    public static final Creator<PrognozaMeteo> CREATOR = new Creator<PrognozaMeteo>() {
        @Override
        public PrognozaMeteo createFromParcel(Parcel in) {
            return new PrognozaMeteo(in);
        }

        @Override
        public PrognozaMeteo[] newArray(int size) {
            return new PrognozaMeteo[size];
        }
    };

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public float getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(float temperatura) {
        this.temperatura = temperatura;
    }

    public String getDescriere() {
        return descriere;
    }

    public void setDescriere(String descriere) {
        this.descriere = descriere;
    }

    @Override
    public String toString() {
        return "PrognozaMeteo{" +
                "data='" + data + '\'' +
                ", temperatura=" + temperatura +
                ", descriere='" + descriere + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(data);
        dest.writeFloat(temperatura);
        dest.writeString(descriere);
    }
}
