package com.example.aplicatiebrasov;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

public class AdaptorPrognozaMeteo extends ArrayAdapter<PrognozaMeteo> {

    private int layoutID;

    public AdaptorPrognozaMeteo(@NonNull Context context, int resource, @NonNull List<PrognozaMeteo> objects) {
        super(context, resource, objects);
        this.layoutID = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View view = inflater.inflate(this.layoutID, null);
        PrognozaMeteo prognozaMeteo = getItem(position);

        ((TextView)view.findViewById(R.id.dataPrognozaTV)).setText(prognozaMeteo.getData());
        ((TextView)view.findViewById(R.id.temperaturaPrognozaTV)).setText(Float.toString(prognozaMeteo.getTemperatura()) + " °C");
        ((TextView)view.findViewById(R.id.descrierePrognozaTV)).setText(prognozaMeteo.getDescriere());

        return view;
    }
}
