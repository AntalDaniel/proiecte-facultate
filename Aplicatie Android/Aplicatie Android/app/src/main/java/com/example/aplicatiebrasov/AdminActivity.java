package com.example.aplicatiebrasov;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class AdminActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseObiective = database.getReference();

        final ArrayList<ObiectivTuristic> listaObiective = new ArrayList<>();

        databaseObiective.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listaObiective.clear();

                DataSnapshot obiectiveRef = dataSnapshot.child("obiective");
                Iterable<DataSnapshot> snapshots = obiectiveRef.getChildren();

                for(DataSnapshot snapshot : snapshots){
                    ObiectivTuristic obiectiv = snapshot.getValue(ObiectivTuristic.class);

                    listaObiective.add(obiectiv);
                }

                if(listaObiective.size()>0){
                    findViewById(R.id.populeazaObTurBTN).setEnabled(false);
                }else {
                    findViewById(R.id.populeazaObTurBTN).setEnabled(true);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void deschideMesaje(View view) {
        Intent it = new Intent(this, AdminMesajeActivity.class);
        startActivity(it);
    }

    public void deschideRestaurante(View view) {
        Intent it = new Intent(this, AdminRestauranteActivity.class);
        startActivity(it);
    }

    public void populeazaObiective(View view) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseObiective = database.getReference("obiective");

        String id = databaseObiective.push().getKey();
        ObiectivTuristic ot1 = new ObiectivTuristic(id, "Biserica Neagra", "Biserica Neagră poartă această denumire deoarece în anul 1689 în urma unui incendiu care a cuprins tot orașul, o mare parte din biserică a luat foc. La început lăcașul a purtat denumirea de hramul Sfintei Maria, dar ulterior a fost acceptată această nouă denumire.\n" +
                "\n" +
                "Biserica Neagră este una dintre cele mai reprezentative construcţii de factură gotică din ţara noastră, ridicarea ei fiind realizată în secolele XIV-XV. Este considerată cea mai mare biserică din România, fiind şi cel mai mare lăcaş de cult în stil gotic din sud-estul Europei.\n" +
                "\n" +
                "În momentul inaugurării a primit titlul de „Cea mai mare biserică dintre Viena și Constantinopol', adică de la de la domul Sf. Ştefan din Viena până la Hagia Sophia din actualul Istanbul.", 45.640933, 25.587685);
        databaseObiective.child(id).setValue(ot1);

        id = databaseObiective.push().getKey();
        ObiectivTuristic ot2 = new ObiectivTuristic(id, "Strada Sforii", "Strada Sforii a devenit în ultimii ani unul dintre cele mai vizitate obiective turistice ale orașului Brașov.\n" +
                "\n" +
                "Strada Sforii este localizată în municipiul Brașov, România, între străzile Cerbului și Poarta Șchei. Existența ei este atestată în documentele din secolul al XVII-lea ca un simplu coridor, pentru a ajuta munca pompierilor.\n" +
                "\n" +
                "Lățimea Străzii Sforii variază între 1,11 și 1,35 m, ceea ce îi conferă titlul de cea mai îngustă stradă a orașului Brașov. Strada Sforii, cu o lungime de 80 metri, reflectă tendințele de urbanizare a Brașovului medieval.", 45.639574, 25.588563);
        databaseObiective.child(id).setValue(ot2);

        id = databaseObiective.push().getKey();
        ObiectivTuristic ot3 = new ObiectivTuristic(id, "Prima Scoala Romaneasca", "În interiorul curții Bisericii Sfântul Nicolae din cartierul istoric Scheii Brașovului vei regăsi Prima Școală Românească. Clădirea este un obiectiv turistic deoarece aici au avut loc primele cursuri de limba română, în anul 1583 și tot aici s-au tipărit primele cărți de limba română.\n" +
                "\n" +
                "Actuala clădire datează din anul 1760, fiind declarată monument istoric. În prezent edificiul adăpostește „Muzeul Prima Școală Românească”, sub conducerea filologului Vasile Oltean.", 45.63586, 25.581255);
        databaseObiective.child(id).setValue(ot3);

        id = databaseObiective.push().getKey();
        ObiectivTuristic ot4 = new ObiectivTuristic(id, "Piata Sfatului", "Dacă ajungi în Brașov nu ai cum să ratezi o plimbare în Piața Sfatului. Este unul dintre cele mai vizitate obiective turistice și datorită faptului că aici sunt organizate foarte des evenimente artistice, culturale, concerte în aer liber, târguri, etc.\n" +
                "\n" +
                "Piața a fost însă construită în anul 1420 și în incinta ei se afla primăria orașului, având sediul în Casa Sfatului, ce a fost transformată mai târziu în muzeu de istorie. Evenimentele importante se desfășurau în piața încăpătoare, aici fiind judecate și vrăjitoarelor, cărora li se aplicau pedepse publice, urmărite cu mare interes de comunitate.", 45.642108, 25.589107);
        databaseObiective.child(id).setValue(ot4);

        id = databaseObiective.push().getKey();
        ObiectivTuristic ot5 = new ObiectivTuristic(id, "Muntele Tampa", "Muntele Tâmpa este unul dintre obiectivele turistice principale ale orașului Brașov.\n" +
                "\n" +
                "Tâmpa este un munte care aparține de masivul Postăvaru, localizat în sudul Carpaților Orientali (mai precis în Carpații de Curbură) și înconjurat aproape în totalitate de municipiul Brașov.\n" +
                "\n" +
                "Muntele este alcătuit în principal din formațiuni calcaroase formate în urma procesului de încrețire a scoarței terestre. Înălțimea maximă atinsă este de 960m (după unele surse 995m), la aproape 400m deasupra orașului. Mare parte a sa (150 ha) este instituită ca rezervație naturală de tip florisitc, faunistic și peisagistic.", 45.634666, 25.594313);
        databaseObiective.child(id).setValue(ot5);

        id = databaseObiective.push().getKey();
        ObiectivTuristic ot6 = new ObiectivTuristic(id, "Muzeul de Arta", "Muzeul de Artă găzduiește expoziţii temporare şi o gamă variată de manifestări culturale (concerte, lansări de carte, mese rotunde, conferinţe etc).\n"
                +"Expoziţia permanentă a muzeului, Galeria Naţională, reuneşte o selecţie de lucrări reprezentative pentru arta plastică modernă din spaţiul românesc (pictură şi sculptură). În cadrul expoziţiei sunt incluse piese care ilustrează evoluţia picturii transilvănene din secolele XVIII-XIX şi a artei româneşti din intervalul cuprins între prima jumătate a secolului XIX şi perioada postbelică. Alături de operele maeştrilor artei româneşti figurează în expunere şi creaţiile artiştilor activi la Brașov, de la portretele patriciatului săsesc la lucrările artiştilor contemporani Brașoveni. ", 45.644983, 25.593750);
        databaseObiective.child(id).setValue(ot6);

        Toast.makeText(this, "Obiectivele turistice au fost adaugate in Firebase!", Toast.LENGTH_SHORT).show();
    }
}
