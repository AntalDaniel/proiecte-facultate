package com.example.aplicatiebrasov;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface RestaurantDAO {

    @Insert
    public void insertRestaurant(Restaurant restaurant);

    @Query("select * from Restaurante;")
    public List<Restaurant> selectAllRestaurante();

    @Query("delete from Restaurante;")
    public void deleteAllRestaurante();

    @Query("delete from Restaurante where id = :id;")
    public void deleteRestaurantById(int id);

    @Query("select * from Restaurante where id = :id")
    public Restaurant selectRestaurantByID(int id);

    @Query("update Restaurante set avgRating = :avgRating where id = :idRest")
    public void updateAvgRatingRestaurantById(float avgRating, int idRest);

    @Query("update Restaurante set nume = :nume, adresa = :adresa, telRezervari = :telRezervari where id = :idRest")
    public void updateRestaurantById(int idRest, String nume, String adresa, String telRezervari);
}
