package com.example.aplicatiebrasov;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

public class AdaptorRestaurant extends ArrayAdapter<Restaurant> {

    private int layoutID;

    public AdaptorRestaurant(@NonNull Context context, int resource, @NonNull List<Restaurant> objects) {
        super(context, resource, objects);
        this.layoutID = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(getContext());
        View view = inflater.inflate(this.layoutID, null);
        Restaurant restaurant = getItem(position);

        TextView tvNume = view.findViewById(R.id.numeRestTV);
        TextView tvAdresa = view.findViewById(R.id.adresaRestTV);
        TextView tvTelefon = view.findViewById(R.id.telefonRestTV);
        RatingBar rbRating = view.findViewById(R.id.ratingRestRB);

        tvNume.setText(restaurant.getNume());
        tvAdresa.setText((restaurant.getAdresa()));
        tvTelefon.setText(restaurant.getTelRezervari());
        rbRating.setRating((float)restaurant.getAvgRating());

        return view;
    }
}
