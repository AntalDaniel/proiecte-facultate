package com.example.aplicatiebrasov;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ObiectivDetaliatAtcivity extends AppCompatActivity {

    private ObiectivTuristic ob;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_obiectiv_detaliat_atcivity);

        Intent it = getIntent();
        TextView tvNume = findViewById(R.id.numeObiectivTV);
        TextView tvDescriere = findViewById(R.id.descriereObiectivTv);

        ob = it.getParcelableExtra("obiectiv");
        tvNume.setText(ob.getNume());
        tvDescriere.setText(ob.getDescriere());
    }

    public void deschideHarta(View view) {
        Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
        intent.putExtra("obiectiv", ob);
        startActivity(intent);
    }
}
