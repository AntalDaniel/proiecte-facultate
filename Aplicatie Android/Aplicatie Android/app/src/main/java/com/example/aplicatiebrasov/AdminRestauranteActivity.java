package com.example.aplicatiebrasov;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AdminRestauranteActivity extends AppCompatActivity {

    private AppDatabase database = null;
    private List<Restaurant> listaRestaurante;
    private int requestCode = 901;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_restaurante);
        database = Room.databaseBuilder(this, AppDatabase.class, "AppDatabase").allowMainThreadQueries().fallbackToDestructiveMigration().build();

        Toast.makeText(this, "Click for Edit", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        citireDate();

    }

    public void deleteAllRestautants(View view) {
        database.restaurantDAO().deleteAllRestaurante();
        citireDate();
    }

    public void citireDate(){
        listaRestaurante = database.restaurantDAO().selectAllRestaurante();

        AdaptorRestaurant adaptor = new AdaptorRestaurant(getApplicationContext(), R.layout.restaurant_item_layout, listaRestaurante);
        ListView lv = findViewById(R.id.restauranteAdmin_LV);
        lv.setAdapter(adaptor);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), AdminGestioneazaRestaurant.class);
                intent.putExtra("idRest", (listaRestaurante.get(position)).getId());
                intent.putExtra("isEdit", true);
                startActivityForResult(intent, requestCode);
            }
        });

        if(listaRestaurante.size()>0){
            findViewById(R.id.deleteAllRestBTN).setEnabled(true);
            findViewById(R.id.raportTXTRestBTN).setEnabled(true);
            findViewById(R.id.barchartRestBTN).setEnabled(true);
        } else {
            findViewById(R.id.deleteAllRestBTN).setEnabled(false);
            findViewById(R.id.raportTXTRestBTN).setEnabled(false);
            findViewById(R.id.barchartRestBTN).setEnabled(false);
        }
    }

    public void adaugaRestaurant(View view) {
        Intent intent = new Intent(getApplicationContext(), AdminGestioneazaRestaurant.class);
        intent.putExtra("isEdit", false);
        startActivity(intent);
    }

    public void raportRestauranteTXT(View view) {

        String rezultat = null;
        for(Restaurant r : listaRestaurante){
            rezultat += "Nume: " + r.getNume() + "\n" +
                    "Adresa: " + r.getAdresa() + "\n" +
                    "Tel: " + r.getTelRezervari() + "\n" +
                    "AvgRating: " + String.valueOf(r.getAvgRating());
            List<Recenzie> recenzii = new ArrayList<>();
            recenzii = database.recenzieDAO().selectRecenziiByID(r.getId());
            if(recenzii.size()>0){
                rezultat += "\nRecenzii: \n";
                for(Recenzie re : recenzii){
                    rezultat += "\t" + re.getMesaj() + "(Rating: " + String.valueOf(re.getStars()) +")\n";
                }
            }
            rezultat += "\n";
        }


        FileOutputStream fos = null;
        try {
            fos = openFileOutput("raportRestaurante.txt", MODE_PRIVATE);
            fos.write(rezultat.getBytes());
            Toast.makeText(this, "Fisier TXT generat cu succes!", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(fos!=null){
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void deschideBarchartRest(View view) {
        BarChart barChart = new BarChart(this, listaRestaurante);
        setContentView(barChart);
    }
}
