package com.example.aplicatiebrasov;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Mesaje")
public class Mesaj {

    @PrimaryKey(autoGenerate = true)
    private int id;
    @NonNull
    private String numePrenume;
    @NonNull
    private String email;
    @NonNull
    private String mesaj;
    private Boolean newsletter;

    public Mesaj(@NonNull String numePrenume, @NonNull String email, @NonNull String mesaj, Boolean newsletter) {
        this.numePrenume = numePrenume;
        this.email = email;
        this.mesaj = mesaj;
        this.newsletter = newsletter;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public String getNumePrenume() {
        return numePrenume;
    }

    public void setNumePrenume(@NonNull String numePrenume) {
        this.numePrenume = numePrenume;
    }

    @NonNull
    public String getEmail() {
        return email;
    }

    public void setEmail(@NonNull String email) {
        this.email = email;
    }

    @NonNull
    public String getMesaj() {
        return mesaj;
    }

    public void setMesaj(@NonNull String mesaj) {
        this.mesaj = mesaj;
    }

    public Boolean getNewsletter() {
        return newsletter;
    }

    public void setNewsletter(Boolean newsletter) {
        this.newsletter = newsletter;
    }

    @Override
    public String toString() {
        return "Mesaj{" +
                "id=" + id +
                ", numePrenume='" + numePrenume + '\'' +
                ", email='" + email + '\'' +
                ", mesaj='" + mesaj + '\'' +
                ", newsletter=" + newsletter +
                '}';
    }
}
