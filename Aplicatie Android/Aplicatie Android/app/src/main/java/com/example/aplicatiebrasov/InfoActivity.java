package com.example.aplicatiebrasov;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class InfoActivity extends AppCompatActivity {

    private AppDatabase database = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        database = Room.databaseBuilder(this, AppDatabase.class, "AppDatabase").allowMainThreadQueries().fallbackToDestructiveMigration().build();

        SharedPreferences sp = getSharedPreferences("user", MODE_PRIVATE);
        ((EditText)findViewById(R.id.info_NumePrenume)).setText(sp.getString("nume_user", ""));
        ((EditText)findViewById(R.id.info_Email)).setText(sp.getString("email_user", ""));
        ((EditText)findViewById(R.id.info_Mesaj)).setText(sp.getString("mesaj_user", ""));
        ((CheckBox)findViewById(R.id.info_noutati_CB)).setChecked(sp.getBoolean("newsletter_user", false));
    }

    @Override
    protected void onStop() {
        super.onStop();
        SharedPreferences sp = getSharedPreferences("user", MODE_PRIVATE);
        String nume = ((EditText)findViewById(R.id.info_NumePrenume)).getText().toString();
        String email = ((EditText)findViewById(R.id.info_Email)).getText().toString();
        String mesaj = ((EditText)findViewById(R.id.info_Mesaj)).getText().toString();
        Boolean newsletter = ((CheckBox)findViewById(R.id.info_noutati_CB)).isChecked();

        SharedPreferences.Editor editor = sp.edit();
        editor.putString("nume_user", nume);
        editor.putString("email_user", email);
        editor.putString("mesaj_user", mesaj);
        editor.putBoolean("newsletter_user", newsletter);
        editor.commit();
    }

    public void trimiteMesaj(View view) {
//        Toast.makeText(this, "Mesajul va fi salvat in baza de date!", Toast.LENGTH_SHORT).show();
//        Toast.makeText(this, "Aceasta functionalitate va fi implementata in curand!", Toast.LENGTH_SHORT).show();

        EditText numePrenume_ET = findViewById(R.id.info_NumePrenume);
        String numePrenume = numePrenume_ET.getText().toString();
        EditText email_ET = findViewById(R.id.info_Email);
        String email = email_ET.getText().toString();
        EditText text_ET = findViewById(R.id.info_Mesaj);
        String text = text_ET.getText().toString();
        CheckBox newsletter_CB = findViewById(R.id.info_noutati_CB);
        Boolean newsletter = newsletter_CB.isChecked();
        if((numePrenume.matches("")) || (email.matches("")) || (text.matches(""))){
            Toast.makeText(this, "Toate campurile sunt obligatorii!", Toast.LENGTH_SHORT).show();
        }else{
            Mesaj mesaj = new Mesaj(numePrenume, email, text, newsletter);
            database.mesajDAO().insertMesaj(mesaj);

            List<Mesaj> listaMesaje = database.mesajDAO().selectAllMesaje();
            Toast.makeText(this, "Mesajul a fost trimis cu succes!", Toast.LENGTH_SHORT).show();
//            Toast.makeText(this, "Total mesaje in BD: " + listaMesaje.size(), Toast.LENGTH_SHORT).show();

            numePrenume_ET.setText("");
            email_ET.setText("");
            text_ET.setText("");
            newsletter_CB.setChecked(false);
        }
    }
}
