package com.example.aplicatiebrasov;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RestauranteActivity extends AppCompatActivity {

    private AppDatabase database = null;
    List<Restaurant> listaRestaurante;
    private int pozitieSelectata;
    private int requestCode = 301;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurante);

        database = Room.databaseBuilder(this, AppDatabase.class, "AppDatabase").allowMainThreadQueries().fallbackToDestructiveMigration().build();

    }

    @Override
    protected void onResume() {
        super.onResume();
        listaRestaurante = new ArrayList<>();
        listaRestaurante = database.restaurantDAO().selectAllRestaurante();

        if(listaRestaurante.size()>0) {
            AdaptorRestaurant adaptor = new AdaptorRestaurant(getApplicationContext(), R.layout.restaurant_item_layout, listaRestaurante);
            ListView lv = findViewById(R.id.restaurante_lista_LV);
            lv.setAdapter(adaptor);

            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(getApplicationContext(), RecenzieRestaurantActivity.class);
                    intent.putExtra("idRest", (listaRestaurante.get(position)).getId());
                    startActivityForResult(intent, requestCode);
                }
            });
            Toast.makeText(this, "Dati clik pentru a adauga o recenzie!", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "Restaurantele se aduaga din Admin!", Toast.LENGTH_SHORT).show();
        }
    }
}
