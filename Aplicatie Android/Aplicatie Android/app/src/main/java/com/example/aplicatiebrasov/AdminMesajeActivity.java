package com.example.aplicatiebrasov;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AdminMesajeActivity extends AppCompatActivity {

    private AppDatabase database = null;
    private ListView lv;
    private List<Mesaj> listaMesaje;
    private List<String> listaMesajeTXT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_mesaje);

        database = Room.databaseBuilder(this, AppDatabase.class, "AppDatabase").allowMainThreadQueries().fallbackToDestructiveMigration().build();

        citireDate();

        if(listaMesaje.size()>0){
            Toast.makeText(this, "LonClick pentru a sterge un mesaj!", Toast.LENGTH_LONG).show();
        }
    }

    public void deleteAllMsgs(View view) {
        database.mesajDAO().deleteAllMesaje();
        citireDate();
    }

    public void citireDate(){

        listaMesaje = database.mesajDAO().selectAllMesaje();
        listaMesajeTXT = new ArrayList<>();
        for(Mesaj m : listaMesaje){
            listaMesajeTXT.add("Nume: " + m.getNumePrenume() + "\n" +
                    "Email: " + m.getEmail() + "\n" +
                    "Newsletter: " + m.getNewsletter() + "\n" +
                    "Mesaj: " + m.getMesaj());
        }
        ListView lv = findViewById(R.id.mesaje_LV);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listaMesajeTXT);
        lv.setAdapter(adapter);

        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                database.mesajDAO().deleteMesajById(listaMesaje.get(position).getId());
                citireDate();
                return false;
            }
        });

        if(listaMesaje.size()>0){
            findViewById(R.id.deleteAllMsgBTN).setEnabled(true);
            findViewById(R.id.pieChartBTN).setEnabled(true);
            findViewById(R.id.raportTXTMsgBTN).setEnabled(true);
        } else {
            findViewById(R.id.deleteAllMsgBTN).setEnabled(false);
            findViewById(R.id.pieChartBTN).setEnabled(false);
            findViewById(R.id.raportTXTMsgBTN).setEnabled(false);
        }
    }

    public void raportMesajeTXT(View view) {
        FileOutputStream fos = null;
        try {
            fos = openFileOutput("raportMesaje.txt", MODE_PRIVATE);
            for(String s : listaMesajeTXT){
                fos.write(s.getBytes());
                fos.write(("\n \n").getBytes());
            }
            Toast.makeText(this, "Fisier TXT generat cu succes!", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(fos!=null){
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void deschidePieChart(View view) {
        int yes= 0;
        int no = 0;
        for(Mesaj m : listaMesaje){
            if(m.getNewsletter()){
                yes++;
            } else {
                no++;
            }
        }
        PieChart pc = new PieChart(this, yes, no);
        setContentView(pc);
    }
}
