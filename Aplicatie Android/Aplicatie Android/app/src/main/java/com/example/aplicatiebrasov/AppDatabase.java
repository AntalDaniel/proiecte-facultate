package com.example.aplicatiebrasov;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Mesaj.class, Restaurant.class, Recenzie.class}, version = 5, exportSchema = true)
public abstract class AppDatabase extends RoomDatabase {

    public abstract MesajDAO mesajDAO();
    public abstract RestaurantDAO restaurantDAO();
    public abstract RecenzieDAO recenzieDAO();
}
