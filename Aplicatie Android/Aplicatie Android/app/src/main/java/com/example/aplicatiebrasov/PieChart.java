package com.example.aplicatiebrasov;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

public class PieChart extends View {

    private int yes;
    private int no;

    public PieChart(Context context, int yes, int no) {
        super(context);
        this.yes = yes;
        this.no = no;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        float suma = yes+no;
        float start = -90;
        Paint instrument = new Paint();
        instrument.setTextSize(100);
        //Desenam YES
        instrument.setColor(Color.rgb(0,255,0));
        float grad = ((float)yes*360)/suma;
        canvas.drawArc(100, 100,500, 500, start, grad, true, instrument);
        canvas.drawText("DA", 650, 200, instrument);
        start+=grad;
        //Desenam NO
        instrument.setColor(Color.rgb(255,0,0));
        grad = ((float)no*360)/suma;
        canvas.drawArc(100, 100,500, 500, start, grad, true, instrument);
        canvas.drawText("NU", 650, 300, instrument);
    }
}
