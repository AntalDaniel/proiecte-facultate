package com.example.aplicatiebrasov;

import android.os.Parcel;
import android.os.Parcelable;

public class ObiectivTuristic implements Parcelable {

    private String id;
    private String nume;
    private String descriere;
    private double lat;
    private double lng;

    public ObiectivTuristic() {
    }

    public ObiectivTuristic(String id, String nume, String descriere, double lat, double lng) {
        this.id = id;
        this.nume = nume;
        this.descriere = descriere;
        this.lat = lat;
        this.lng = lng;
    }

    protected ObiectivTuristic(String id, Parcel in, double lat, double lng) {
        this.id = id;
        nume = in.readString();
        descriere = in.readString();
        this.lat = lat;
        this.lng = lng;
    }

    protected ObiectivTuristic(Parcel in) {
        id = in.readString();
        nume = in.readString();
        descriere = in.readString();
        lat = in.readDouble();
        lng = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(nume);
        dest.writeString(descriere);
        dest.writeDouble(lat);
        dest.writeDouble(lng);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ObiectivTuristic> CREATOR = new Creator<ObiectivTuristic>() {
        @Override
        public ObiectivTuristic createFromParcel(Parcel in) {
            return new ObiectivTuristic(in);
        }

        @Override
        public ObiectivTuristic[] newArray(int size) {
            return new ObiectivTuristic[size];
        }
    };

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    protected ObiectivTuristic(String id, Parcel in) {
        this.id = id;
        nume = in.readString();
        descriere = in.readString();
        lat = in.readDouble();
        lng = in.readDouble();
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getDescriere() {
        return descriere;
    }

    public void setDescriere(String descriere) {
        this.descriere = descriere;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
