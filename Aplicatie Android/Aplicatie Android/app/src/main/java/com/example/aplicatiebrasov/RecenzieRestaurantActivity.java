package com.example.aplicatiebrasov;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class RecenzieRestaurantActivity extends AppCompatActivity {

    private AppDatabase database = null;
    int idRest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recenzie_restaurant);

        database = Room.databaseBuilder(this, AppDatabase.class, "AppDatabase").allowMainThreadQueries().fallbackToDestructiveMigration().build();

        Intent it = getIntent();
        idRest = it.getIntExtra("idRest", -1);

        Restaurant restaurant = database.restaurantDAO().selectRestaurantByID(idRest);
        ((TextView)findViewById(R.id.numeRestTV)).setText(restaurant.getNume());
        afisareRecenzii();
    }

    void afisareRecenzii(){
        List<Recenzie> recenzii = database.recenzieDAO().selectRecenziiByID(idRest);
        List<String> rec = new ArrayList<>();
        for(Recenzie r : recenzii){
            rec.add(r.getMesaj() + "\n" + "Rating: "+ r.getStars());
        }
        ListView lv = findViewById(R.id.recenziiLV);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, rec);
        lv.setAdapter(adapter);
    }

    public void saveRecenzie(View view) {

        EditText recenzieET = findViewById(R.id.recenzieRestET);
        RatingBar ratingBar = findViewById(R.id.ratingRestRB);

        if((recenzieET.getText().toString().matches("")) || (ratingBar.getRating() == 0)){
            Toast.makeText(this, "Toate campurile sunt obligatorii!", Toast.LENGTH_SHORT).show();
        }else {
            String recenzie = recenzieET.getText().toString();
            float rating = ratingBar.getRating();

            Recenzie r = new Recenzie(idRest, rating, recenzie);
            database.recenzieDAO().insertRecenzie(r);
            afisareRecenzii();
            recenzieET.setText("");
            ratingBar.setRating(0.0f);
            Toast.makeText(this, "Recenzia a fost salvata!", Toast.LENGTH_SHORT).show();

            //Update recenzie Restaurant}
            float avgStars = database.recenzieDAO().selectAvgStarsByID(idRest);
            database.restaurantDAO().updateAvgRatingRestaurantById(avgStars, idRest);
        }

    }
}
