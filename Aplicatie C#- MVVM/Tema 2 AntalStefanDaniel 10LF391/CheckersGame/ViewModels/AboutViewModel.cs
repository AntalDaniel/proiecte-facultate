﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckersGame.ViewModels
{
    public class AboutViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public AboutViewModel()
        {
            Developer = "Nume: Antal Stefan-Daniel";
            Email = "Email: Stefan.antal@student.unitbv.ro";
            Group = "Grupa: 10LF391";
            Rules= "The opponent with the darker pieces moves first. Pieces may only move one diagonal space forward (towards their opponents pieces) in the beginning of the game. Pieces must stay on the dark squares. To capture an opposing piece, jump over it by moving two diagonal spaces in the direction of the the opposing piece.";
        }

        private string developer;
        public string Developer
        {
            get { return developer; }
            set { developer = value; OnPropertyChanged("Developer"); }
        }

        private string email;
        public string Email
        {
            get { return email; }
            set { email = value; OnPropertyChanged("Email"); }
        }

        private string group;
        public string Group
        {
            get { return group; }
            set { group = value; OnPropertyChanged("Group"); }
        }
        private string rules;

        public string Rules
        {
            get { return rules; }
            set { rules = value; OnPropertyChanged("Rules"); }
        }

    }
}
