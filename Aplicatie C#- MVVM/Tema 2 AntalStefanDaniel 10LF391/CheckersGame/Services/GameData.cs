﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CheckersGame.Data
{
    public static class GameData
    {
        private static int defaultStartMoves = 4;
        public static string GameInfo(string playerName, int playerPossibleMoves, bool isPieceSelected)
        {
            if (playerPossibleMoves < 0) playerPossibleMoves = defaultStartMoves;

            string message = "Player to move: " + playerName;
            if(!isPieceSelected)
                message+="\nCan select "+playerPossibleMoves+" pieces!";
            else message += "\nCan move this piece in " + playerPossibleMoves +" places!";
            return message;
        }

        public static string GameOver(string opponentName)
        {
            MessageBox.Show("Congratulations!");
            return opponentName + " win! \nPress 'New Game' to play again!";
        }
    }
}
