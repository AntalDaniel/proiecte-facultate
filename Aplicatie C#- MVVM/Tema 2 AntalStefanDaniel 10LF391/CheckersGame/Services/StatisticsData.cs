﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckerGame.Services
{
    public class StatisticsData
    {

        public StatisticsData()
        {

        }

        public string[] ReadStatistics()
        {
            int counter = 0;
            string[] lines = new string[6];
            string line;

             
            System.IO.StreamReader file = new System.IO.StreamReader(@"D:\Anul 2\Semestrul 2\Medii vizuale de Programare\Teme\Tema 2 AntalStefanDaniel 10LF391\CheckersGame\Statistici.txt");
            while ((line = file.ReadLine()) != null)
            {
                lines[counter]=(line);
                counter++;
            }

            file.Close();
            return lines;
        }

        public void WriteStatistics(bool RedWin, bool WhiteWin)
        {
            string[] lines = ReadStatistics();

            if(RedWin)
            {
                lines[0] = (Int32.Parse(lines[0]) + 1).ToString();
               
            }
           
            if (WhiteWin)
            {
                lines[1] = (Int32.Parse(lines[1]) + 1).ToString();
               
            }
           
          
            string docPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            
            using (StreamWriter outputFile = new StreamWriter(Path.Combine(docPath, @"D:\Anul 2\Semestrul 2\Medii vizuale de Programare\Teme\Tema 2 AntalStefanDaniel 10LF391\CheckersGame\Statistici.txt")))
            {
                foreach (string line in lines)
                    outputFile.WriteLine(line);
            }
        }
    }
}
